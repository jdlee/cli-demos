package com.steeplesoft.clis.crest;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.tomitribe.crest.Main;
import org.tomitribe.crest.SystemEnvironment;
import org.tomitribe.crest.api.Command;
import org.tomitribe.crest.api.Default;
import org.tomitribe.crest.api.Option;
import org.tomitribe.crest.api.Required;

public class CrestGlassFish {

    public static void main(String... args) throws Exception {
        final Main main = new Main(CrestGlassFish.class);
        main.main(new SystemEnvironment(), new String[] {
            "deploy",
            "--archive=/home/jdlee/src/glassfish/bg/glassfish/appserver/admingui/devtests/src/test/resources/test.war"
        });
    }

    @Command(value = "deploy")
    public String hello(
            @Option("host") @Default("localhost") String host,
            @Option("port") @Default("4848") int port,
            @Option("archive") @Required URI archive) {
        Client client = ClientBuilder.newClient();
        WebTarget target = getBaseTarget(client, host, port).path("applications").path("application");
        MultivaluedMap props = new MultivaluedHashMap();
        props.add("id", archive.toString());
        props.add("force", "true");

        Response r = target.request(MediaType.APPLICATION_JSON)
                .header("X-Requested-By", "airlift")
                .post(Entity.entity(props, MediaType.APPLICATION_FORM_URLENCODED), Response.class);

        try {
            if (r.getStatus() != Response.Status.OK.getStatusCode()) {
                final JSONObject entity = new JSONObject(r.readEntity(String.class));
                System.err.println("Deploy failed: " + entity.getString("message"));
                throw new RuntimeException();
            } else {
                return "The application has been deployed.";
            }
        } catch (JSONException ex) {
            Logger.getLogger(CrestGlassFish.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "error";
    }

    protected WebTarget getBaseTarget(Client client, String host, int port) {
        return client.target("http://" + host + ":" + port).path("/management").path("domain");
    }
}
