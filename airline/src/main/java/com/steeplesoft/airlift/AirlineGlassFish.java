package com.steeplesoft.airlift;

import io.airlift.command.Arguments;
import io.airlift.command.Cli;
import io.airlift.command.Cli.CliBuilder;
import io.airlift.command.Command;
import io.airlift.command.Help;
import io.airlift.command.Option;
import io.airlift.command.OptionType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class AirlineGlassFish {
    public static void main(String[] args) {
        CliBuilder<Runnable> builder = Cli.<Runnable>builder("glassfish")
                .withDescription("Sample airlift-based CLI for managing GlassFish")
                .withDefaultCommand(Help.class)
                .withCommands(Help.class, Deploy.class);

        Cli<Runnable> gitParser = builder.build();

        gitParser.parse(args).run();
    }

    public abstract static class GlassFishCommand implements Runnable {

        @Option(type = OptionType.GLOBAL, name = "-h", description = "host")
        public String host = "localhost";
        @Option(type = OptionType.GLOBAL, name = "-p", description = "port")
        public int port = 4848;

        protected final Client client;

        protected GlassFishCommand() {
            client = ClientBuilder.newClient();
        }

        protected WebTarget getBaseTarget() {
            return client.target("http://" + host + ":" + port).path("/management").path("domain");
        }

    }

    @Command(name = "deploy", description = "Deploy an application")
    public static class Deploy extends GlassFishCommand {

        @Arguments(description = "Archives to deploy", required = true)
        List<String> fileNames;

        @Override
        public void run() {
            WebTarget target = getBaseTarget().path("applications").path("application");
            MultivaluedMap props = new MultivaluedHashMap();
            props.add("id", fileNames.get(0));
            props.add("force", "true");
            Response r = target.request(MediaType.APPLICATION_JSON)
                    .header("X-Requested-By", "airlift")
                    .post(Entity.entity(props, MediaType.APPLICATION_FORM_URLENCODED), Response.class);

            try {
                if (r.getStatus() != Status.OK.getStatusCode()) {
                    final JSONObject entity = new JSONObject(r.readEntity(String.class));
                    System.err.println("Deploy failed: "  + entity.getString("message"));
                    throw new RuntimeException();
                } else {
                    System.out.println("The application has been deployed.");
                }
            } catch (JSONException ex) {
                Logger.getLogger(AirlineGlassFish.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
